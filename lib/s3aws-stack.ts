import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
// Create an S3 Bucket and add bucket properties like versioning and encryption
import * as s3 from 'aws-cdk-lib/aws-s3';


export class S3AwsStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const bucket = new s3.Bucket(this, 's3miniproject', {
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED,
    })

    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'S3AwsQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
