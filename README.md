# S3 Bucket Creation with AWS CDK

This project demonstrates the process of creating an Amazon S3 bucket with versioning and encryption features using the AWS Cloud Development Kit (CDK). The project is developed using the local terminal, leveraging AWS IAM for permissions, and utilizes CodeWhisperer for generating CDK code.

## Prerequisites

Before starting, ensure you have the following:
- An AWS account
- AWS CLI installed and configured
- Node.js and npm installed
- AWS CDK Toolkit installed

## Setup and Configuration

1. **AWS IAM User Setup**:
   - Create a new IAM user with `IAMFullAccess`, `AmazonS3FullAccess`, and `AWSLambda_FullAccess`  and `AmazonEC2ContainerRegistryFullAccess` permissions.
   - Add inline policies for `CloudFormation` and `Systems Manager` with full access.

2. **Project Initialization**:
   - Create a new project directory: `mkdir YOUR_PROJECT_NAME`
   - Navigate to the directory: `cd YOUR_PROJECT_NAME`
   - Initialize a CDK application: `cdk init app --language=typescript`

## Development

1. **Code Generation**:
   - Utilize `CodeWhisperer` to generate CDK code for an S3 bucket with versioning and encryption. Use prompt like `// make an S3 bucket` and `// make an S3 bucket and enable versioning and encryption` in `\lib\mini-proj-3-stack.ts` to generate the code.
   - Update `\lib\mini-proj-3-stack.ts` and `\bin\mini-proj-3.ts` as per project requirements.

2. **Build the Project**:
   - Compile TypeScript to JavaScript: `npm run build`

3. **Deploy the Stack**:
   - Deploy to your AWS account: `cdk deploy` (try `cdk bootstrap` if deploying for the first time)

## Verification

- After deployment, verify the creation of the S3 bucket in the AWS S3 console.
  - S3 bucket overview 
    ![overview](overview.png)
 
- Check the bucket's properties for versioning and encryption.
  - S3 bucket properties of versioning and encryption
    ![versioning](versioning.png)
    ![encryption](encryption.png)
